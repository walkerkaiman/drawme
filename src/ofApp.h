#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxCv.h"
#include "ofxGui.h"
#include "ofxVectorGraphics.h"
#include <unistd.h>

using namespace ofxCv;
using namespace cv;

class ofApp : public ofBaseApp {
    
    public:
        void setup();
        void update();
        void draw();
        int getCameraID(ofVideoGrabber grabber, String name);
        void displayTimer(String timerValue);
        void displayMessage(String message);
        void displayBlobs();
        void beginTimer();
        void resizeBlob();
        void saveBlobSVG(String filename);
        void keyPressed(int key);
        void exit();
        string ofxGetSerialString(ofSerial &serial, char until);
    
        ofVideoGrabber cam;
        ofxCvColorImage colorImage;
        ofxCvGrayscaleImage grayImage;
        ofxCvContourFinder contourFinder;
        ofImage roiImage;
    
        ofxPanel gui;
        ofParameter<int> blockSize;
        ofParameter<int> blobSizeMin;
        ofParameter<int> blobSizeMax;
        ofParameter<int> nSimplify;
        ofParameter<int> nGaussianBlur;
        ofParameter<int> nBlobs;
        ofParameter<float> timerAmount;
    
        ofTrueTypeFont timerDisplay, instructionsDisplay;
        ofRectangle ROI;
    
        ofxVectorGraphics vectorOutput;
    
        int frameWidth, frameHeight;
        float startTime, remainingTime;
        bool isShowingTimer, isShowingGUI;
        String axibotPlot, axibotManual;
    
        ofSerial serial;
};
