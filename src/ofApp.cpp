#include "ofApp.h"

using namespace ofxCv;
using namespace cv;

string ofApp::ofxGetSerialString(ofSerial &serial, char until) {
    static string str;
    stringstream ss;
    char ch;
    int ttl=1000;
    while ((ch=serial.readByte())>0 && ttl-->0 && ch!=until) {
        ss << ch;
    }
    str+=ss.str();
    if (ch==until) {
        string tmp=str;
        str="";
        return ofTrim(tmp);
    } else {
        return "";
    }
}

int ofApp::getCameraID(ofVideoGrabber grabber, string name) {
    vector<ofVideoDevice> devices = grabber.listDevices();
    
    for (vector<ofVideoDevice>::iterator it = devices.begin(); it != devices.end(); ++it) {
        if (it->deviceName == name) {
            ofLog(OF_LOG_NOTICE, "Using Camera: " + name);
            return it->id;
            break;
        }
    }
    
    ofLog(OF_LOG_ERROR, "Could not find the requested camera...");
    
    if (devices.size() > 0) {
        ofLog(OF_LOG_ERROR, "Using default camera: " + devices.at(0).deviceName);
        return 0;
    }else {
        ofLog(OF_LOG_ERROR, "No camera attached...");
        ofExit();
        return -1;
    }
}

void ofApp::displayTimer(String countdownValue) {
    ofPushStyle();
    ofSetColor(ofColor::red);
    timerDisplay.drawString(countdownValue, frameWidth, frameHeight+50);
    ofPopStyle();
}

void ofApp::displayMessage(String message) {
    ofPushStyle();
    ofSetColor(ofColor::red);
    instructionsDisplay.drawString(message, frameWidth, frameHeight+50);
    ofPopStyle();
}

void ofApp::displayBlobs(){
    ofPushStyle();
    ofSetColor(ofColor::red);
    ofNoFill();
    
    for(int blob = 0; blob < contourFinder.nBlobs; blob++){
        ofBeginShape();
        for(int point = 0; point < contourFinder.blobs[blob].pts.size(); point++){
            ofVertex(contourFinder.blobs[blob].pts[point]);
        }
        ofEndShape();
    }
    ofPopStyle();
}

void ofApp::beginTimer() {
    isShowingTimer = true;
    startTime = ofGetElapsedTimef();
}

// This function used the region of interest and extracts the pixel data
// from the frame with those coordinates. The ROI is then saved with a
// unique name to the bin/data directory.

void ofApp::saveBlobSVG(String filename) {
    String filenameSVG = filename.append(".svg");

    ofBeginSaveScreenAsSVG(filename, false, false, ROI);
    ofPushMatrix();
    ofTranslate(-ROI.getLeft(), -ROI.getTop());
    
    for(int blobIndex = 0; blobIndex < contourFinder.nBlobs; blobIndex++) {
        vector<ofPoint> points = contourFinder.blobs[blobIndex].pts;
        
        ofBeginShape();
        ofPushStyle();
        ofNoFill();
        ofSetColor(ofColor::white);
        
        for(int pointIndex = 0; pointIndex < points.size(); pointIndex++) {
            ofVertex(points[pointIndex].x, points[pointIndex].y);
        }
        
        ofEndShape();
        ofPopStyle();
    }
    
    ofPopMatrix();
    
    ofEndSaveScreenAsSVG();
    ofLogNotice() << "Saved: " + filename;
    
    // Call terminal commands to the AxiDraw
    ofLogNotice() << ofSystem(axibotPlot + ofToDataPath(filenameSVG, true));
    ofSystem(axibotManual + "disable_motors");
}

void ofApp::setup() {
    
    
    frameWidth = 640;
    frameHeight = 480;

    isShowingTimer = false;
    isShowingGUI = true;
    
    ofSetWindowShape(frameWidth*2, frameHeight+100);
    ofSetWindowPosition(0, 0);
    ofSetBackgroundColor(ofColor::black);
    ofSetBackgroundAuto(true);
    
    ROI.set(0, 0, 0, 0);
    
    ofXml settingsXML;
    settingsXML.load(ofToDataPath("settings.xml"));
    settingsXML.setTo("//CameraName");
    cam.setDeviceID(ofApp::getCameraID(cam, settingsXML.getValue()));
	cam.setup(frameWidth, frameHeight);
    
    settingsXML.setTo("//SerialDevice");
    String serialName = settingsXML.getValue();
    
    settingsXML.setTo("//Baud");
    int baudRate = settingsXML.getIntValue();
    
    serial.setup(serialName, baudRate);
    
    // Set up the GUI elements.
    ofXml CVsettingsXML;
    CVsettingsXML.load(ofToDataPath("settings_cv.xml"));
    
    gui.setup();
    
    CVsettingsXML.setTo("//Block_Size");
    gui.add(blockSize.set("Block Size", CVsettingsXML.getIntValue(), 10.0, 200.0));
    
    CVsettingsXML.setTo("//Blob_Size_Min");
    gui.add(blobSizeMin.set("Blob Size Min", CVsettingsXML.getIntValue(), 0.0, (frameWidth*frameHeight)/2));
    
    CVsettingsXML.setTo("//Blob_Size_Max");
    gui.add(blobSizeMax.set("Blob Size Max", CVsettingsXML.getIntValue(), 0.0, (frameWidth*frameHeight)/2));
    
    CVsettingsXML.setTo("//Simplify");
    gui.add(nSimplify.set("Simplify", CVsettingsXML.getIntValue(), 0, 8));
    
    CVsettingsXML.setTo("//Blur_Amount");
    gui.add(nGaussianBlur.set("Blur Amount", CVsettingsXML.getIntValue(), 0, 20));
    
    CVsettingsXML.setTo("//Number_of_Shapes");
    gui.add(nBlobs.set("Number of Shapes", CVsettingsXML.getIntValue(), 1, 30));
    
    CVsettingsXML.setTo("//Timer_Amount");
    gui.add(timerAmount.set("Timer Amount", CVsettingsXML.getFloatValue(), 0.0, 10.0));
    
    ofTrueTypeFont::setGlobalDpi(72);
    timerDisplay.loadFont("arial.ttf", 200, true, true);
    instructionsDisplay.loadFont("arial.ttf", 30, true, true);
    
    colorImage.allocate(frameWidth, frameHeight);
    grayImage.allocate(frameWidth, frameHeight);
    roiImage.allocate(frameWidth, frameHeight, OF_IMAGE_GRAYSCALE);
    
    startTime = 0;
    remainingTime = timerAmount;
    axibotPlot = "/usr/local/bin/axibot plot ";
    axibotManual = "/usr/local/bin/axibot manual ";
    
    ofSystem(axibotManual + "disable_motors");
}

void ofApp::update() {
    if(ofxGetSerialString(serial, '\n') == "Released"){
        if(isShowingGUI) {
            isShowingGUI = false;
        }else {
            beginTimer();
        }
    }
    
	cam.update();
    
    if(cam.isFrameNew()) {
        // Get the new frame from the camera and set the color image from the new frame.
        colorImage.setFromPixels(cam.getPixels());
        
        // Convert the color image to a grayscale image.
        grayImage = colorImage;
        
        // Threshold the grayscale image. Block size must be an odd number.
        if(blockSize % 2 == 0) {
            grayImage.adaptiveThreshold(blockSize+1);
        }else {
            grayImage.adaptiveThreshold(blockSize);
        }
        
        // Dialate the grayscale image simplify shapes. Amount set by GUI.
        for (int i = 0; i < nSimplify; i++) {
            grayImage.erode_3x3();
        }
        for (int i = 0; i < nSimplify; i++) {
            grayImage.dilate_3x3();
        }
        
        for(int i = 0; i < nGaussianBlur; i++) {
            grayImage.blurGaussian();
        }
        
        // Find the blobs in the grayscale image.
        contourFinder.findContours(grayImage, blobSizeMin, blobSizeMax, nBlobs, true, true);
        
        // Update ROI
        if(contourFinder.nBlobs > 0) {
            int left = 10000;
            int top = 100000;
            int right = 0;
            int bottom = 0;
            
            for(int i = 0; i < contourFinder.nBlobs; i++) {
                ofRectangle blobPos = contourFinder.blobs[i].boundingRect;
                
                if(blobPos.getLeft() < left) {
                    left = blobPos.getLeft();
                }
                if(blobPos.getTop() < top) {
                    top = blobPos.getTop();
                }
                if(blobPos.getRight() > right) {
                    right = blobPos.getRight();
                }
                if(blobPos.getBottom() > bottom) {
                    bottom = blobPos.getBottom();
                }
            }
            ROI.set(left, top, right-left, bottom-top);
            roiImage.setFromPixels(grayImage.getPixels());
            roiImage.crop(ROI.getLeft(), ROI.getTop(), ROI.getWidth(), ROI.getHeight());
        }
	}
    
    if(isShowingTimer) {
        remainingTime = timerAmount - (ofGetElapsedTimef() - startTime); // Update the timer.
        
        if(remainingTime <= 0) { // If the timer has run out, save the blobs and reset the timer.
            saveBlobSVG("tempROI");
            isShowingTimer = false;
            isShowingGUI = true;
            startTime = 0;
            remainingTime = timerAmount;
        }
    }
}

void ofApp::draw() {
    ofBackground(ofColor::black);
    
    // Display the color frame
    if(isShowingGUI) {
        colorImage.draw(0, 0);
    }
    
    // Display the blobs over the threshold frame.
    displayBlobs();
    
    if(isShowingGUI && contourFinder.nBlobs > 0) {
        roiImage.draw(frameWidth, 0);
    }
    
    if(isShowingTimer) {
        displayTimer(ofToString(remainingTime, 1));
    }else {
        if(isShowingGUI) {
            displayMessage("Hit button to set parameters");
        }
        else {
            displayMessage("Hit button to draw \nHit space to restart");
        }
    }
    
    // Display the GUI on top of everything.
    if(isShowingGUI) {
        gui.draw();
    }
}

void ofApp::keyPressed(int key) {
    if(key == ' '){
        isShowingGUI = true;
    }
}

void ofApp::exit() {
    gui.saveToFile(ofToDataPath("settings_cv.xml"));
    ofSystem(axibotManual + "disable_motors");
}
